#!/bin/bash

sudo add-apt-repository main
sudo add-apt-repository universe
sudo add-apt-repository restricted
# ZFS
sudo apt-add-repository multiverse

# Postgres
wget -q -O - https://gerardo.herzig:vZfk9GfVX6kCGmHy@apt.enterprisedb.com/edb-deb.gpg.key  | sudo apt-key add -
# Setup the repository (edit the username/password as appropriate) # We must change the Gerardo credentials and try to use a Ongres ones.
# We need encrypt this credentials into the vault (?)
sudo sh -c 'echo "deb https://gerardo.herzig:vZfk9GfVX6kCGmHy@apt.enterprisedb.com/$(lsb_release -cs)-edb/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/edb-$(lsb_release -cs).list'

for attempt in $(seq 1 3)
do
    sudo apt-get install -y apt-transport-https
    sudo apt-get update  -y --fix-missing && \
    sudo apt-get install -y nano vim htop ec2-api-tools && \
    sudo apt-get install -y python3 && \
    sudo apt-get install -y python3-pip && \
    sudo apt-get install -y jq && \
    sudo apt-get install -y edb-as11 pgbouncer edb-as11-server-client && \
    sudo apt-get install -y zfsutils-linux && \
    sudo apt-get install -y awscli
    sudo apt-get install -y sysstat
done

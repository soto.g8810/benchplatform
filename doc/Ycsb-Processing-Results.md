

```
cat doc/ycsb_final_benchs | cut -d "=" -f2 | sed 's|s3://benchplatform-results.s3.amazonaws.com/||' | xargs -I {} bin/getResults {}
awk -F '=' '/ycsb/ {print $2}' doc/sysbench_final_benchs  | awk -F '/' '{print $4" "$5}' | xargs -I {}  bin/processYcsbResults {} 
```
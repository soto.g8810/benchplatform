# Commands for executing benchmarks


## Sysbench

Example for building the commands dynamically:

```
bin/listConfigs | grep sysbench | grep -ve '^snapshot' | awk '{print $1}' | xargs -I {} echo make TARGET="module.{}" APPROVE="y" apply
```

```
make TARGET=module.postgres-sysbench-xfs-fit APPROVE=y destroy &&\
make TARGET=module.postgres-sysbench-zfs-fit APPROVE=y destroy && \
make TARGET=module.mongo-sysbench-xfs-fit APPROVE=y destroy && \
make TARGET=module.mongo-sysbench-zfs-fit APPROVE=y destroy && \
make TARGET=module.pgbouncer-sysbench-xfs-fit APPROVE=y destroy && \
make TARGET=module.pgbouncer-sysbench-zfs-fit APPROVE=y destroy && \
make TARGET=module.postgres-sysbench-xfs-xl APPROVE=y destroy && \
make TARGET=module.postgres-sysbench-zfs-xl APPROVE=y destroy && \
make TARGET=module.mongo-sysbench-xfs-xl APPROVE=y destroy && \
make TARGET=module.mongo-sysbench-zfs-xl APPROVE=y destroy && \
make TARGET=module.pgbouncer-sysbench-xfs-xl APPROVE=y destroy && \
make TARGET=module.pgbouncer-sysbench-zfs-xl APPROVE=y destroy

make TARGET=module.postgres-sysbench-xfs-fit APPROVE=y apply &&\
make TARGET=module.postgres-sysbench-zfs-fit APPROVE=y apply && \
make TARGET=module.mongo-sysbench-xfs-fit APPROVE=y apply && \
make TARGET=module.mongo-sysbench-zfs-fit APPROVE=y apply && \
make TARGET=module.pgbouncer-sysbench-xfs-fit APPROVE=y apply && \
make TARGET=module.pgbouncer-sysbench-zfs-fit APPROVE=y apply && \
make TARGET=module.postgres-sysbench-xfs-xl APPROVE=y apply && \
make TARGET=module.postgres-sysbench-zfs-xl APPROVE=y apply && \
make TARGET=module.mongo-sysbench-xfs-xl APPROVE=y apply && \
make TARGET=module.mongo-sysbench-zfs-xl APPROVE=y apply && \
make TARGET=module.pgbouncer-sysbench-xfs-xl APPROVE=y apply && \
make TARGET=module.pgbouncer-sysbench-zfs-xl APPROVE=y apply

```

## YCSB snapshots


```
make TARGET=snapshot-postgres-ycsb-xfs-fit prepare && \
make TARGET=snapshot-mongo-ycsb-xfs-fit prepare

make TARGET=snapshot-postgres-ycsb-xfs-xl prepare && \
make TARGET=snapshot-mongo-ycsb-xfs-xl prepare
```

```
make TARGET=module.snapshot-postgres-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.snapshot-mongo-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.snapshot-postgres-ycsb-xfs-xl APPROVE=y apply && \
make TARGET=module.snapshot-mongo-ycsb-xfs-xl APPROVE=y apply

make TARGET=module.snapshot-postgres-ycsb-xfs-fit APPROVE=y destroy && \
make TARGET=module.snapshot-mongo-ycsb-xfs-fit APPROVE=y destroy && \
make TARGET=module.snapshot-postgres-ycsb-xfs-xl APPROVE=y destroy && \
make TARGET=module.snapshot-mongo-ycsb-xfs-xl APPROVE=y destroy
```

## YCSB Runs

```
make TARGET=postgres-ycsb-xfs-fit prepare && \
make TARGET=mongo-ycsb-xfs-fit prepare && \
make TARGET=postgres-ycsb-xfs-xl prepare && \
make TARGET=mongo-ycsb-xfs-xl prepare && \
make TARGET=pgbouncer-ycsb-xfs-fit prepare && \
make TARGET=pgbouncer-ycsb-xfs-xl prepare


make TARGET=module.postgres-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.mongo-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.postgres-ycsb-xfs-xl APPROVE=y apply && \
make TARGET=module.mongo-ycsb-xfs-xl APPROVE=y apply


make TARGET=module.postgres-ycsb-xfs-fit APPROVE=y destroy && \
make TARGET=module.mongo-ycsb-xfs-fit APPROVE=y destroy && \
make TARGET=module.postgres-ycsb-xfs-xl APPROVE=y destroy && \
make TARGET=module.mongo-ycsb-xfs-xl APPROVE=y destroy


make TARGET=module.postgres-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.mongo-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.postgres-ycsb-xfs-xl APPROVE=y apply && \
make TARGET=module.mongo-ycsb-xfs-xl APPROVE=y apply && \
make TARGET=module.pgbouncer-ycsb-xfs-fit APPROVE=y apply && \
make TARGET=module.pgbouncer-ycsb-xfs-xl APPROVE=y apply

```

### Custom

make TARGET=postgres-custom-xfs prepare && \
make TARGET=mongotx-custom-xfs prepare && \
make TARGET=pgbouncer-custom-xfs prepare 


make TARGET=module.postgres-custom-xfs APPROVE=y destroy && \
make TARGET=module.mongotx-custom-xfs APPROVE=y destroy && \
make TARGET=module.pgbouncer-custom-xfs APPROVE=y destroy 

make TARGET=module.postgres-custom-xfs APPROVE=y apply && \
make TARGET=module.mongotx-custom-xfs APPROVE=y apply && \
make TARGET=module.pgbouncer-custom-xfs APPROVE=y apply


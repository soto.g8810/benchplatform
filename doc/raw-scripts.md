
## Mongo

### sysbench

```
sysbench $HOME/mongo_benchmark/sysbench/sysbench-mongodb-lua/oltp-mongo.lua  --tables=10  --threads=10  --table-size=1000000 --mongodb-db=sbtesfit --mongodb-host=localhost --mongodb-port=27017  --rand-type=pareto  prepare
sysbench $HOME/mongo_benchmark/sysbench/sysbench-mongodb-lua/oltp-mongo.lua  --tables=50  --threads=10  --table-size=10000000 --mongodb-db=sbtesthighLoad  --mongodb-host=localhost --mongodb-port=27017  --rand-type=pareto  prepare
sysbench $HOME/mongo_benchmark/sysbench/sysbench-mongodb-lua/oltp-mongo.lua  --tables=100  --threads=10 --table-size=100000000 --mongodb-db=sbtestXL  --mongodb-host=localhost --mongodb-port=27017  --rand-type=pareto  prepare
```

### YCSB

```
$HOME/ycsb/ycsb-0.15.0/bin/ycsb load mongodb-async -s -threads 10 -p recordcount=10000000   -p mongodb.url=mongodb://localhost:27017/ycsbfit 
$HOME/ycsb/ycsb-0.15.0/bin/ycsb load mongodb-async -s -threads 10 -p recordcount=200000000  -p mongodb.url=mongodb://localhost:27017/ycsbhighLoad 
$HOME/ycsb/ycsb-0.15.0/bin/ycsb load mongodb-async -s -threads 10 -p recordcount=10000000000  p mongodb.url=mongodb://localhost:27017/ycsbXL 
```


## Postgres

### sysbench 

```
sysbench --db-driver=pgsql --report-interval=1  --threads=25 --oltp-table-size=1000000    --oltp-tables-count=10  --pgsql-host=localhost  --pgsql-port=5432 --pgsql-user=sysbench      --pgsql-password=1. --pgsql-db=sbtestfit  /usr/share/sysbench/tests/include/oltp_legacy/oltp.lua   prepare 
sysbench --db-driver=pgsql --report-interval=1  --threads=25 --oltp-table-size=10000000   --oltp-tables-count=50  --pgsql-host=localhost  --pgsql-port=5432 --pgsql-user=sysbench      --pgsql-password=1. --pgsql-db=sbtesthighLoad  /usr/share/sysbench/tests/include/oltp_legacy/oltp.lua   prepare  
sysbench --db-driver=pgsql --report-interval=1  --threads=25 --oltp-table-size=100000000  --oltp-tables-count=100 --pgsql-host=localhost  --pgsql-port=5432 --pgsql-user=sysbench     --pgsql-password=1. --pgsql-db=sbtestXL  /usr/share/sysbench/tests/include/oltp_legacy/oltp.lua   prepare  
```

### YCSB

```
/opt/ycsb-0.15.0/bin/ycsb load  jdbc  -P /opt/ycsb-0.15.0/jdbc-binding/conf/db.highLoad.5432.properties -cp /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar -p threadcount=20 -p recordcount=10000000
/opt/ycsb-0.15.0/bin/ycsb load  jdbc  -P /opt/ycsb-0.15.0/jdbc-binding/conf/db.highLoad.5432.properties -cp /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar -p threadcount=20 -p recordcount=200000000  
/opt/ycsb-0.15.0/bin/ycsb load  jdbc  -P /opt/ycsb-0.15.0/jdbc-binding/conf/db.highLoad.5432.properties -cp /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar -p threadcount=20 -p recordcount=10000000000
```
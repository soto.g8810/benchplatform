
#
# Global Makefile
#

SHELL=/bin/bash

# Does not work, if condition
# ifndef TARGET 
# ifneq ($*, setup)
# $(error TARGET is not set)
# endif
# endif

ifndef TFCMD
TFCMD=plan
endif

ifndef MODULE_TEMPLATE
MODULE_TEMPLATE=compute_set.template
endif

ifdef TARGET 
OPTTARGET=-target="$(TARGET)"
endif 

ifdef DEFVARS
OPTDEFVARS=-var-file="$(DEFVARS)"
endif

ifdef APPROVE
OPTAUTOAPPROVE=-auto-approve
endif

# ifdef TARGET
# TARGETCONF= ./terraform/etc/$@/$(TARGET).tfvars
# include $(TARGETCONF)
# endif

define help
First, try to setup your key and bucket:
make setup			# Only performed once
make init      # Only performed once
make plan 
make apply     # This will apply the existing infra 


# Prepare the infra (prepare does only need 1 time, it creates a .tf
# in the terraform/ folder ) and apply only a single module
make TARGET="snapshot-postgres-sysbench-xfs-fit" prepare
make TARGET="module.snapshot-postgres-sysbench-xfs-fit" [plan|apply]

Otherwise, you can prepare all your benchs and issue: `make apply`

# Prepare the infra for the run (prepare does only need 1 time, it creates a .tf
# in the terraform/ folder )
make TARGET="postgres-sysbench-xfs-fit" prepare
make TARGET="module.postgres-sysbench-xfs-fit" plan
make TARGET="module.postgres-sysbench-xfs-fit" apply


endef
export help
# BUCKETNAME=benchplat


# Here we enable actions:
PHONY: image setup load run help output preoutput

help:
	@echo "$$help"

setup:
	source .env && bin/setup   
	# && \
	# python /usr/local/bin/aws s3 mb s3://$(BUCKETNAME) && \
	# PRIVATEKEY=${HOME}/.ssh/id_rsa_${PROJECT} && \
	# PUBLICKEY=${PRIVATEKEY}.pub && \
	# [ ! -f ${PRIVATEKEY} ] && ssh-keygen -t ecdsa -b 521 -N "" -f ${PRIVATEKEY} && \
	# ssh-keygen -yf ${PRIVATEKEY} > ${PUBLICKEY}

image:
	source .env && cd packer/ ; \
	[[ $(TARGET) == "all" ]] &&  make all || make $(TARGET)-build

init:
	source .env && cd terraform && terraform init

# base:
# 	source .env && cd terraform && terraform $(TFCMD) -var-file='etc/default.tfvars'  

# base:
# 	source .env && cd terraform/base && \
# 	terraform $(TFCMD) 

prepare:
	. .env && cd terraform && \
		sed 's/_TARGET_/$(TARGET)/g' templates/$(MODULE_TEMPLATE) > $(TARGET).tf && \
		terraform init

output:
	source .env && cd terraform && terraform output  $(TARGET)

output-json:
	source .env && cd terraform && terraform output -json $(TARGET)

refresh:
	source .env && cd terraform && terraform refresh $(OPTDEFVARS)

%:
	# apply or plan
	source .env && cd terraform  && \
		terraform $* $(OPTDEFVARS) $(OPTTARGET) $(OPTAUTOAPPROVE)

# %:
# 	# -var 'project=${project}'
# 	source .env && cd terraform && \
# 	terraform $(TFCMD) \
# 		-var 'action=$*' \
# 		-var 'target=$(TARGET)' \
# 		-var-file='etc/$*/$(TARGET).tfvars'

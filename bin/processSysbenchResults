#! /bin/bash

# Requirements:
# - A ready to write database endpoint.
# - Already downloaded results using `getResults`

# Notes:
# As not all the runs did succeed (or were part of tests, and
# probably we'll see garbarge), this tool will proces per family benchmard
# and _benchmarkid _ basis. Metadata has been  included in recent versions,
# but we want to upload as much as possible.

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

source ${SCRIPTPATH}/functions
source ${SCRIPTPATH}/../.env

S3NAME="s3://$(getGlobalAttr project)-results.s3.amazonaws.com"
localResults=$(getGlobalAttr localResults)

PSQL=$(which psql)
# Database URL default
ENDPOINT="postgresql://postgres@localhost:5432/benchplatform"

PSQLENDPOINT="${PSQL} ${ENDPOINT}"
# It would be nice to implement partitions in child tables.
# ERROR:  cannot create partitioned table as inheritance child
# This is the header of each of the tables
DDLParent="
    -- parent
    target text,
    id uuid,
    node text,
    -- end parent
"

DDLSar="
-- WIP
CREATE TABLE iostat (
Date,Time,%user,%nice,%system,%iowait,%steal,%idle,Device,r/s,w/s,rkB/s,wkB/s,rrqm/s,wrqm/s,%rrqm,%wrqm,r_await,w_await,aqu-sz,rareq-sz,wareq-sz,svctm,%util,Device,r/s,w/s,rkB/s,wkB/s,rrqm/s,wrqm/s,%rrqm,%wrqm,r_await,w_await,aqu-sz,rareq-sz,wareq-sz,svctm,%util,Device,r/s,w/s,rkB/s,wkB/s,rrqm/s,wrqm/s,%rrqm,%wrqm,r_await,w_await,aqu-sz,rareq-sz,wareq-sz,svctm,%util,Device,r/s,w/s,rkB/s,wkB/s,rrqm/s,wrqm/s,%rrqm,%wrqm,r_await,w_await,aqu-sz,rareq-sz,wareq-sz,svctm,%util,Device,r/s,w/s,rkB/s,wkB/s,rrqm/s,wrqm/s,%rrqm,%wrqm,r_await,w_await,aqu-sz,rareq-sz,wareq-sz,svctm,%util
) INHERITS (parent) PARTITION BY (target);

"

DDLSysbench="
-- WIP
CREATE TABLE aggregated_runs (
    threads int,
    transactions bigint,
    deadlocks   double precision,
    readwrite   double precision,
    min     double precision,
    avg     double precision,
    max     double precision,
    percentile text
);


CREATE TABLE bysecond_runs (
    -- parent
    target text,
    id uuid,
    node text,
    -- end parent
    type text,
    sec int,
    threads int,
    tps double precision,
    qps double precision,
    read double precision,
    write double precision,
    o double precision,
    latency_ms_95th double precision,
    errors double precision,
    reconn double precision);
CREATE INDEX ON bysecond_runs (target,id,node,sec);
"

DDLYCSB="
"

DDLCustom="

"

DDLGithub="
"

loadBySecond_runs=


function initiate {
    $PSQLENDPOINT <<EOF
    ${DDLSysbench}
EOF
}

function parseBySecondSysbench {
    target=${1}
    id=${2}
    node=${3}
    file=${4}
    type=$(basename $file | egrep -o "[0-9]{4}")
    if [[ $target =~ .*postgres.* ]]; then
        awk '/^\[/ { gsub("s","",$2); print "'''${target}''','''${id}''','''${node}''','''${type}''',"$2","$5","$7","$9","$11","$14","$16","$18}' $file \
        | tr -d ")" | tr "/" ","  > ${file}.bysecond.csv
    elif [[ $target =~ .*mongo.* ]]; then 
        awk '/^\[/ { gsub("s","",$2); print "'''${target}''','''${id}''','''${node}''','''${type}''',"$2","$5","$5","$7",0,0,0,"$10",0,0"}' $file \
        | tr -d ")" | tr "/" ","  > ${file}.bysecond.csv
    elif [[ $target =~ .*pgbouncer.* ]]; then 
        awk '/^\[/ { gsub("s","",$2); print "'''${target}''','''${id}''','''${node}''','''${type}''',"$2","$5","$7","$9","$11","$14","$16","$18}' $file \
        | tr -d ")" | tr "/" ","  > ${file}.bysecond.csv
    fi
}

function iterateSysbenchTargetId {
    for file in $(ls ${localResults}/${1}/${2}/*/*.out)
    do
        node=`basename $(dirname $file)`
        parseBySecondSysbench ${1} ${2} ${node} ${file}
        $PSQLENDPOINT <<EOF
        COPY bysecond_runs FROM '${file}.bysecond.csv' WITH(FORMAT CSV);
EOF
    done
}

if [[ $1 == '-i' ]] ; then
    initiate
    exit
fi

iterateSysbenchTargetId $1 $2 ${3:-}
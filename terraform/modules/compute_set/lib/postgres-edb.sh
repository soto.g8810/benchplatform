#!/bin/bash

source /usr/local/bin/default.sh
RESULTS_DIR="$(getTag Resultsdir)"


cp /home/ubuntu/conf/postgres-edb/postgresql.conf /etc/edb-as/11/main/postgresql.conf
cp /home/ubuntu/conf/postgres-edb/pg_hba.conf /etc/edb-as/11/main/pg_hba.conf


grep MemTotal /proc/meminfo | \
awk '{memkb=($2/1024)/4} END {printf("\nshared_buffers = %.0fMB\n", memkb)}' >> /etc/edb-as/11/main/postgresql.conf

grep MemTotal /proc/meminfo | \
awk '{memkb=($2/1024)-($2/1024)/4} END {printf("\effective_cache_size = %.0fMB\n", memkb)}' >> /etc/edb-as/11/main/postgresql.conf

cp /etc/edb-as/11/main/postgresql.conf ${RESULTS_DIR}


# Check if /opt/data is initialized
chown -R enterprisedb: /opt/data
su enterprisedb -c '/usr/lib/edb-as/11/bin/initdb -D /opt/data' 

# Start postgres over /opr/data in 5432
systemctl ${1} edb-as@11-main.service 

#!/usr/bin/env bash
source /usr/local/bin/default.sh

action=$(getTag Action)
# device=$(getRealDevice)
# device_path="/dev/${device}"

# function try_nvme_create {
#     # TODO fancy lsblk search
#     zpool create -f datadir -m $1 /dev/nvme0n1
#     zfs set mountpoint=$1 /dev/nvme0n1 
# }


umount /mnt 2> /dev/null

mkdir $2 2> /dev/null

if [ $action == "load" ]
then
    zpool create -f datadir -m $2 $1 || zpool create -f datadir -m $2  /dev/nvme0n1 #${device_path}  # || try_nvme_create $2
else  
    zpool import datadir
fi

zfs set mountpoint=$2 datadir 


### changes ZFS ### 
zfs set compression=lz4 datadir
zfs set recordsize=8k datadir # XXX! Mongo might need a different size
zfs set atime=off datadir
zfs set primarycache=metadata datadir
zfs get atime,compression,recordsize,primarycache datadir

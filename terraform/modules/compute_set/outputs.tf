output "client_public_ip" {
    value = ["${aws_instance.client.*.public_ip}"]
}


output "datanode_load_public_ip" {
    value = ["${aws_instance.datanode-load.*.public_ip}"]
}

output "datanode_run_public_ip" {
    value = ["${aws_instance.datanode-run.*.public_ip}"]
}

output "resultsUrl" {
    value = "s3://${var.s3uri}/${var.target}/${random_uuid.benchid.result}"
}

# output "test_config" {
#     value = "${var.config}"
# }
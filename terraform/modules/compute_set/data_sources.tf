
data "aws_ami" "datanode_image" {
    most_recent      = true
    owners           = ["self"]

    filter {
        name = "name"
        values = ["${var.config["datanode_image_name"]}"]
    }   
}


data "aws_ami" "client_image" {
    most_recent      = true
    owners           = ["self"]

    filter {
        name = "name"
        values = ["${var.config["client_image_name"]}"]
    }   
}


data "template_file" "datanode_cloud_init" {
  template = "${file("${path.module}/templates/${var.config["action"]}/datanode/cloud-config.yml.tpl")}"

  vars {
    entrypoint          = "${base64encode(file("${path.module}/files/${var.config["action"]}/datanode/entrypoint.sh"))}"
    finish              = "${base64encode(file("${path.module}/files/${var.config["action"]}/datanode/finish.sh"))}"
    libdefault          = "${base64encode(file("${path.module}/lib/default.sh"))}"
    fsscript            = "${base64encode(file("${path.module}/lib/${var.config["fs"]}.sh"))}"
    dbenginescript      = "${base64encode(file("${path.module}/lib/${var.config["dbengine"]}.sh"))}"
    triggerscript       = "${base64encode(file("${path.module}/files/${var.config["action"]}/client/${var.config["trigger_script"]}.sh"))}"
    device_name         = "${var.config["device_name"]}"
    datadir             = "${var.config["datadir"]}"
    fs                  = "${var.config["fs"]}"
    dbengine            = "${var.config["dbengine"]}"
    sshuser             = "${var.globalconfig["sshuser"]}"
    resultsdir          = "${local.pretags["Resultsdir"]}"
  }
}

data "template_cloudinit_config" "datanode_config" {
  gzip          = true
  base64_encode = true


  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.datanode_cloud_init.rendered}"
  }
}

/*
Client
*/

data "template_file" "client_cloud_init" {
  template = "${file("${path.module}/templates/${var.config["action"]}/client/cloud-config.yml.tpl")}"

  vars {
    entrypoint         = "${base64encode(file("${path.module}/files/${var.config["action"]}/client/entrypoint.sh"))}"
    finish              = "${base64encode(file("${path.module}/files/${var.config["action"]}/client/finish.sh"))}"
    libdefault         = "${base64encode(file("${path.module}/lib/default.sh"))}"
    triggerscript      = "${base64encode(file("${path.module}/files/${var.config["action"]}/client/${var.config["trigger_script"]}.sh"))}"
    device_name         = "${var.config["device_name"]}"
    sshuser             = "${var.globalconfig["sshuser"]}"
    resultsdir          = "${local.pretags["Resultsdir"]}"
  }
}

data "template_cloudinit_config" "client_config" {
  gzip          = true
  base64_encode = true  // (*1)

  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.client_cloud_init.rendered}"
  }
}


data "aws_ebs_snapshot" "ebs_volume" {
  count       = "${var.config["action"] == "run" ? 1 : 0}"

  most_recent = true
  owners      = ["self"]

  filter {
    name   = "tag:Name"
    values = ["${var.config["snapshotname"]}"]
  }
}
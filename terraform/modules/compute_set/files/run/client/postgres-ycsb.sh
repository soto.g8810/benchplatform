#!/usr/bin/env bash

source /usr/local/bin/default.sh

tables=$(getTag Tables)
numrows=$(getTag Numrows)
dbname=$(getTag Dbname)
dbhost=$(getDbip)
threads=$(getTag Threads)
cycles=$(getTag Cycles)
duration=$(getTag Duration)
threads=$(getTag Threads)
step=$(getTag Step)
RESULTS_DIR="$(getTag Resultsdir)"

# cp /home/${sshuser}/conf/${tool}/workloads/*${loadsize} /opt/ycsb-0.15.0/workloads/

echo "
recordcount=${numrows}
operationcount=0
maxexecutiontime=${duration}
workload=com.yahoo.ycsb.workloads.CoreWorkload

readallfields=true
maxscanlength=100000
readproportion=0.5
updateproportion=0.5
scanproportion=0
insertproportion=0

requestdistribution=zipfian
" > ${HOME}/workload5050

echo "
recordcount=${numrows}
operationcount=0
maxexecutiontime=${duration}
workload=com.yahoo.ycsb.workloads.CoreWorkload

readallfields=true
maxscanlength=100000
readproportion=0.95
updateproportion=0.05
scanproportion=0
insertproportion=0

requestdistribution=zipfian
" > ${HOME}/workload9505

# Iterate in cycles

#Not really necessary
#/usr/bin/psql -h ${dbhost} -Upostgres -c "ANALYZE" ${dbname}

for thread_iter in $(seq  ${threads} -${step} 1 | head -${cycles} )
do
echo "Running ${thread_iter}"

/opt/ycsb-0.15.0/bin/ycsb  run  jdbc  \
	-p "db.url=jdbc:postgresql://${dbhost}:5432/${dbname}?user=postgres&reWriteBatchedInserts=true&batchupdateapi=true" \
    -P ${HOME}/workload5050 \
    -cp /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar \
    -p threadcount=${thread_iter} \
    -p recordcount=${numrows} | tee -a  ${RESULTS_DIR}/threads_${thread_iter}-5050.out 

/opt/ycsb-0.15.0/bin/ycsb  run  jdbc  \
	-p "db.url=jdbc:postgresql://${dbhost}:5432/${dbname}?user=postgres&reWriteBatchedInserts=true&batchupdateapi=true" \
    -P ${HOME}/workload9505 \
    -cp /opt/ycsb-0.15.0/jdbc-binding/lib/postgresql-42.2.5.jar \
    -p threadcount=${thread_iter} \
    -p recordcount=${numrows} | tee -a ${RESULTS_DIR}/threads_${thread_iter}-9505.out 
done


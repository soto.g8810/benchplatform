
resource "aws_instance" "client" {
  ami                         = "${data.aws_ami.client_image.image_id}"    #"${data.aws_ami.datanode_image}" # us-west-2
  instance_type               = "${var.config["client_instance_type"]}"

  count                       = "${var.config["factor"]}"

  subnet_id                   = "${var.subnet_id}"
  vpc_security_group_ids      = ["${var.vpc_security_group_ids}"]
  associate_public_ip_address = "${var.associate_public_ip_address}"
  iam_instance_profile        = "${var.iam_instance_profile}"

  # availability_zone           = "${var.globalconfig["region"]}a"
  key_name                    = "${var.key_name}"

  # network_interface {
  #   network_interface_id = "${aws_network_interface.foo.id}"
  #   device_index         = 0
  # }

  user_data_base64          = "${data.template_cloudinit_config.client_config.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir /home/${var.globalconfig["sshuser"]}/conf",
    ]

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
      timeout       = "45m"
    }
  }
  provisioner "file" {
    source      = "${path.module}/conf"
    destination = "/home/${var.globalconfig["sshuser"]}"

    connection {
      type        = "ssh"
      user        = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
      timeout       = "45m"
    }
  }


  provisioner "file" {
    source      = "~/.ssh/id_rsa_${var.globalconfig["project"]}"
    destination = "/home/${var.globalconfig["sshuser"]}/.ssh/id_rsa"

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }

  provisioner "file" {
    source      = "~/.ssh/id_rsa_${var.globalconfig["project"]}.pub"
    destination = "/home/${var.globalconfig["sshuser"]}/.ssh/id_rsa.pub"

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/${var.globalconfig["sshuser"]}/.ssh/*",
    ]

    connection {
      type = "ssh"
      user = "${var.globalconfig["sshuser"]}" 
      private_key =  "${file("~/.ssh/id_rsa_${var.globalconfig["project"]}")}"
    }
  }

  lifecycle {
    # Due to several known issues in Terraform AWS provider related to arguments of aws_instance:
    # (eg, https://github.com/terraform-providers/terraform-provider-aws/issues/2036)
    # we have to ignore changes in the following arguments
    ignore_changes = ["private_ip", "root_block_device", "ebs_block_device"]
  }

  tags = "${merge(map("Name", format("client-%s-%d", var.target, count.index+1),
                      "Dbtagname", format("datanode-%s-%d", var.target, count.index+1), 
                      "Dbhost", format("datanode-%s-%d", var.target, count.index+1),
                      "Benchid", "${random_uuid.benchid.result}"
                      ), local.pretags)}"
  # Dbtagname use for information when uploading results
}

